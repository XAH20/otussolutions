﻿using System;

namespace avito
{
    public class Ad
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public DateTime СreatedAt { get; set; }
        public string Type { get; set; }
        public string Text { get; set; }
        public int Price { get; set; }

    }
}
