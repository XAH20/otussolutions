﻿using System;
using System.Collections.Generic;

namespace avito
{
    class Program
    {
        private static Repo _repo = new Repo("Host=localhost;Username=postgres;Password=123;Database=avito");
        public static void Main(string[] args)
        {
            //_repo.Init();
            //_repo.DataSeed();
            ShowAll();
            Run();
        }

        private static void Run()
        {
            while (true)
            {
                Console.WriteLine("Для добавления объявления введите команду:");
                Console.WriteLine("<ads> <user_id>,<category_id>,<type>,<text>,<price>");
                Console.WriteLine("Для выхода введите \"exit\"");

                var cmd = Console.ReadLine();

                if (cmd == "exit")
                {
                    return;
                }

                AddRow(cmd);
            }
        }

        private static void ShowAll()
        {
            ShowUsers();
            ShowCategories();
            ShowAds();
        }

        private static void ShowUsers()
        {
            var fields = new List<string> { "id", "name", "password", "email", "phone" };
            var users = _repo.GetAllUsers();
            Console.WriteLine("USERS");
            Console.WriteLine($"{fields[0],3}{fields[1],12}{fields[2],12}{fields[3],20}{fields[4],12}");
            foreach (var user in users)
            {
                Console.WriteLine($"{user.Id,3}{user.Name,12}{user.Password,12}{user.Email,20}{user.Phone,12}");
            }
            Console.WriteLine();
        }

        private static void ShowCategories()
        {
            var fields = new List<string> { "id", "name" };
            var categories = _repo.GetAllCategories();
            Console.WriteLine("CATEGORIES");
            Console.WriteLine($"{fields[0],3}{fields[1],12}");
            foreach (var user in categories)
            {
                Console.WriteLine($"{user.Id,3}{user.Name,12}");
            }
            Console.WriteLine();
        }

        private static void ShowAds()
        {
            var fields = new List<string> { "id", "user_id", "category_id", "created_at", "type", "text", "price" };
            var ads = _repo.GetAllAds();
            Console.WriteLine("ADS");
            Console.WriteLine($"{fields[0],3}{fields[1],12}{fields[2],12}{fields[3],25}{fields[4],8}{fields[5],25}{fields[6],12}");
            foreach (var ad in ads)
            {
                Console.WriteLine($"{ad.Id,3}{ad.UserId,12}{ad.CategoryId,12}{ad.СreatedAt,25}{ad.Type,8}{ad.Text,25}{ad.Price,12}");
            }
            Console.WriteLine();
        }

        private static void AddRow(string cmd)
        {
            try
            {
                var nameTable = cmd.Substring(0, cmd.IndexOf(' '));
                var cmdNameFieldsStr = cmd[(cmd.IndexOf(' ') + 1)..];
                var cmdNameFieldsArray = cmdNameFieldsStr.Split(',');

                if (nameTable == "ads")
                {
                    var ad = new Ad();
                    ad.UserId = int.Parse(cmdNameFieldsArray[0]);
                    ad.CategoryId = int.Parse(cmdNameFieldsArray[1]);
                    ad.СreatedAt = DateTime.Now;
                    ad.Type = cmdNameFieldsArray[2];
                    ad.Text = cmdNameFieldsArray[3];
                    ad.Price = int.Parse(cmdNameFieldsArray[4]);

                    _repo.AddAd(ad);

                    Console.WriteLine("Объявление успешно добавлено!");
                    Console.WriteLine();
                    ShowAds();
                }
                else
                {
                    Console.WriteLine("Введена неправильная команда");
                    Console.WriteLine();
                }
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Введено неверное количество параметров для добавления строки");
                Console.WriteLine();
            }
            catch (ArgumentOutOfRangeException)
            {
                Console.WriteLine("Введена неправильная команда");
                Console.WriteLine();
            }
            catch (FormatException)
            {
                Console.WriteLine("Введена неправильная команда. Значением параметров с числовым типом должно быть число.");
                Console.WriteLine();
            }
            catch (Npgsql.PostgresException e)
            {
                Console.WriteLine("Ошибка при добавлении строки в базу");
                Console.WriteLine(e.Message);
                Console.WriteLine();
            }
        }
    }
}
