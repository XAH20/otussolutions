﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace avito
{
    public class Repo
    {
        private string ConnectionString;

        public Repo(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public NpgsqlConnection GetConnection()
        {
            return new NpgsqlConnection(ConnectionString);
        }

        public void Init()
        {
            CreateTableUsers();
            CreateTableСategory();
            CreateTableAds();
        }

        public void DataSeed()
        {
            AddUsers();
            AddCategories();
            AddAds();
        }

        private void CreateTableUsers()
        {
            using var connection = GetConnection();
            connection.Open();
            var sql = @"
CREATE SEQUENCE users_id_seq;

CREATE TABLE users
(
    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('users_id_seq'),
    name            CHARACTER VARYING(255)      NOT NULL,
    password        CHARACTER VARYING(255)      NOT NULL,
    email           CHARACTER VARYING(255)      NOT NULL,
    phone           CHARACTER VARYING(20)       NOT NULL,
  
    CONSTRAINT users_pkey PRIMARY KEY (id),
    CONSTRAINT users_email_unique UNIQUE (email),
    CONSTRAINT users_phone_unique UNIQUE (phone)
);";

            using var cmd = new NpgsqlCommand(sql, connection);
            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Created USERS table. Affected rows count: {affectedRowsCount}");
        }

        private void CreateTableСategory()
        {
            using var connection = GetConnection();
            connection.Open();
            var sql = @"
CREATE SEQUENCE categories_id_seq;

CREATE TABLE categories
(
    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('categories_id_seq'),
    name            CHARACTER VARYING(255)      NOT NULL,
  
    CONSTRAINT categories_pkey PRIMARY KEY (id)
);";

            using var cmd = new NpgsqlCommand(sql, connection);
            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Created CATEGORIES table. Affected rows count: {affectedRowsCount}");
        }

        private void CreateTableAds()
        {
            using var connection = GetConnection();
            connection.Open();
            var sql = @"
CREATE SEQUENCE ads_id_seq;

CREATE TABLE ads
(
    id              BIGINT                      NOT NULL    DEFAULT NEXTVAL('ads_id_seq'),
    user_id         BIGINT                      NOT NULL,
    category_id     BIGINT                      NOT NULL,
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL,  
    type            CHARACTER VARYING(10)       NOT NULL,
    text            CHARACTER VARYING(255)      NOT NULL,
    price           BIGINT                      NOT NULL,
  
    CONSTRAINT ads_pkey PRIMARY KEY (id),
    CONSTRAINT ads_fk_user_id FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
    CONSTRAINT ads_fk_category_id FOREIGN KEY (category_id) REFERENCES categories(id) ON DELETE CASCADE,
    CONSTRAINT type_check CHECK (type IN ('buy', 'sell'))
);";

            using var cmd = new NpgsqlCommand(sql, connection);
            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Created ADS table. Affected rows count: {affectedRowsCount}");
        }

        private void AddUsers()
        {
            var user1 = new User() 
            { 
                Name = "Константин", 
                Password = "111", 
                Email = "konst@rambler.ru", 
                Phone = "123" 
            };
            AddUser(user1);

            var user2 = new User()
            {
                Name = "Иван",
                Password = "211",
                Email = "ivan@rambler.ru",
                Phone = "223"
            };
            AddUser(user2);

            var user3 = new User()
            {
                Name = "Сергей",
                Password = "311",
                Email = "sergey@rambler.ru",
                Phone = "323"
            };
            AddUser(user3);

            var user4 = new User()
            {
                Name = "Пётр",
                Password = "411",
                Email = "petr@rambler.ru",
                Phone = "423"
            };
            AddUser(user4);

            var user5 = new User()
            {
                Name = "Антон",
                Password = "511",
                Email = "anton@rambler.ru",
                Phone = "523"
            };
            AddUser(user5);
        }

        public void AddUser(User user)
        {
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
INSERT INTO users(name, password, email, phone) 
VALUES (:name, :password, :email, :phone);
";
            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("name", user.Name));
            parameters.Add(new NpgsqlParameter("password", user.Password));
            parameters.Add(new NpgsqlParameter("email", user.Email));
            parameters.Add(new NpgsqlParameter("phone", user.Phone));

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Insert into USERS table. Affected rows count: {affectedRowsCount}");
        }

        private void AddCategories()
        {
            var category1 = new Category()
            {
                Name = "Квартиры"
            };
            AddCategory(category1);
            var category2 = new Category()
            {
                Name = "Автомобили"
            };
            AddCategory(category2);
            var category3 = new Category()
            {
                Name = "Мебель"
            };
            AddCategory(category3);
            var category4 = new Category()
            {
                Name = "Телефоны"
            };
            AddCategory(category4);
            var category5 = new Category()
            {
                Name = "Велосипеды"
            };
            AddCategory(category5);
        }

        public void AddCategory(Category category)
        {
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
INSERT INTO categories(name) 
VALUES (:name);
";
            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("name", category.Name));

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Insert into CATEGORIES table. Affected rows count: {affectedRowsCount}");
        }

        private void AddAds()
        {
            var ad1 = new Ad()
            {
                UserId = 1,
                CategoryId = 1,
                СreatedAt = DateTime.Now,
                Type = "buy",
                Text = "Куплю квартиру",
                Price = 1_000_000
            };
            AddAd(ad1);
            var ad2 = new Ad()
            {
                UserId = 2,
                CategoryId = 1,
                СreatedAt = DateTime.Now,
                Type = "sell",
                Text = "Продам квартиру",
                Price = 2_000_000
            };
            AddAd(ad2);
            var ad3 = new Ad()
            {
                UserId = 3,
                CategoryId = 5,
                СreatedAt = DateTime.Now,
                Type = "sell",
                Text = "Продам велосипед",
                Price = 30_000
            };
            AddAd(ad3);
            var ad4 = new Ad()
            {
                UserId = 3,
                CategoryId = 4,
                СreatedAt = DateTime.Now,
                Type = "sell",
                Text = "Продам телефон",
                Price = 30_000
            };
            AddAd(ad4);
            var ad5 = new Ad()
            {
                UserId = 5,
                CategoryId = 3,
                СreatedAt = DateTime.Now,
                Type = "buy",
                Text = "Куплю диван",
                Price = 1_000
            };
            AddAd(ad5);
        }

        public void AddAd(Ad ad)
        {
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
INSERT INTO ads (user_id, category_id, created_at, type, text, price) 
VALUES (:user_id, :category_id, :created_at, :type, :text, :price);
";
            using var cmd = new NpgsqlCommand(sql, connection);
            var parameters = cmd.Parameters;
            parameters.Add(new NpgsqlParameter("user_id", ad.UserId));
            parameters.Add(new NpgsqlParameter("category_id", ad.CategoryId));
            parameters.Add(new NpgsqlParameter("created_at", ad.СreatedAt));
            parameters.Add(new NpgsqlParameter("type", ad.Type));
            parameters.Add(new NpgsqlParameter("text", ad.Text));
            parameters.Add(new NpgsqlParameter("price", ad.Price));

            var affectedRowsCount = cmd.ExecuteNonQuery().ToString();

            Console.WriteLine($"Insert into ADS table. Affected rows count: {affectedRowsCount}");
        }

        public List<User> GetAllUsers()
        {
            var users = new List<User>();
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
SELECT id, name, password, email, phone FROM users
";

            using var cmd = new NpgsqlCommand(sql, connection);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var user = new User();
                user.Id = reader.GetInt32(0);
                user.Name = reader.GetString(1);
                user.Password = reader.GetString(2);
                user.Email = reader.GetString(3);
                user.Phone = reader.GetString(4);

                users.Add(user);
            }
            return users;
        }

        public List<Category> GetAllCategories()
        {
            var categories = new List<Category>();
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
SELECT id, name FROM categories
";

            using var cmd = new NpgsqlCommand(sql, connection);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var category = new Category();
                category.Id = reader.GetInt32(0);
                category.Name = reader.GetString(1);

                categories.Add(category);
            }
            return categories;
        }

        public List<Ad> GetAllAds()
        {
            var ads = new List<Ad>();
            using var connection = GetConnection();
            connection.Open();

            var sql = @"
SELECT id, user_id, category_id, created_at, type, text, price FROM ads
";

            using var cmd = new NpgsqlCommand(sql, connection);

            var reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                var ad = new Ad();
                ad.Id = reader.GetInt32(0);
                ad.UserId = reader.GetInt32(1);
                ad.CategoryId = reader.GetInt32(2);
                ad.СreatedAt = reader.GetDateTime(3);
                ad.Type = reader.GetString(4);
                ad.Text = reader.GetString(5);
                ad.Price = reader.GetInt32(6);

                ads.Add(ad);
            }
            return ads;
        }
    }
}
