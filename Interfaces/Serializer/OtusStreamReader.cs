﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Interfaces
{
    class OtusStreamReader<T> : IEnumerable<T>, IDisposable
    {
        private readonly Stream stream;
        private readonly OtusXmlSerializer<T[]> serializer;

        public OtusStreamReader(string filename)
        {
            stream = File.OpenRead(filename);
            serializer = new OtusXmlSerializer<T[]>();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IEnumerator<T> GetEnumerator()
        {
            var list = serializer.Deserialize(stream);
            foreach (T item in list)
            {
                yield return item;
            }
            stream.Seek(0, SeekOrigin.Begin);
        }

        public void Dispose()
        {
            stream.Dispose();
        }
    }
}
