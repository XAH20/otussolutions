﻿using System.IO;

namespace Interfaces
{
    public interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(Stream stream);
    }
}
