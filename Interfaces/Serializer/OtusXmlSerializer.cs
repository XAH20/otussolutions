﻿using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using System.IO;
using System.Xml;

namespace Interfaces
{
    public class OtusXmlSerializer<T> : ISerializer<T>
    {
        private static readonly XmlWriterSettings XmlWriterSettings;
        private readonly IExtendedXmlSerializer serializer;

        static OtusXmlSerializer()
        {
            XmlWriterSettings = new XmlWriterSettings { Indent = true };
        }

        public OtusXmlSerializer()
        {
            // https://github.com/ExtendedXmlSerializer/home
            serializer = new ConfigurationContainer()
              .UseAutoFormatting()
              .UseOptimizedNamespaces()
              .EnableImplicitTyping(typeof(T))
              .Create();
        }

        public T Deserialize(Stream stream)
        {
            using XmlReader reader = XmlReader.Create(stream);
            return (T)serializer.Deserialize(reader);
        }

        public string Serialize (T item)
        {
            return serializer.Serialize(XmlWriterSettings, item);
        }
    }
}
