﻿using System;

namespace Interfaces
{
    public class Account : AbstractPerson
    {
        public Account() : base()
        {
        }
        public Account(string firstName, string lastName, DateTime birthDate) : base(firstName, lastName, birthDate)
        {
        }
    }
    
}
