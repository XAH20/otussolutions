﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Interfaces
{
    public class Repository : IRepository<Account>
    {
        public string Path { get; }
        private readonly OtusXmlSerializer<Account[]> _serializer = new OtusXmlSerializer<Account[]>();

        public Repository(string path)
        {
            Path = path;
        }

        public void Add(Account newAccount)
        {
            Account[] accounts;
            if (File.Exists(Path))
            {
                var oldAccounts = Deserialize();
                accounts = new Account[oldAccounts.Length + 1];
                Array.Copy(oldAccounts, accounts, oldAccounts.Length);
                accounts[accounts.Length - 1] = newAccount;
            }
            else
            {
                accounts = new Account[1] { newAccount };
            }
            var accountsSerialized = _serializer.Serialize(accounts);
            File.WriteAllText(Path, accountsSerialized);
        }

        public IEnumerable<Account> GetAll()
        {
            var accounts = Deserialize();
            foreach (Account account in accounts)
            {
                yield return account;
            }
        }

        public Account GetOne(Func<Account, bool> predicate)
        {
            var accounts = Deserialize();
            foreach (Account account in accounts)
            {
                if (predicate(account))
                {
                    return account;
                }
            }
            return null;
        }

        private Account[] Deserialize()
        {
            using var stream = File.OpenRead(Path);
            return _serializer.Deserialize(stream);
        }
    }
}
