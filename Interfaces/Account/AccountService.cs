﻿using System;

namespace Interfaces
{
    public class AccountService : IAccountService
    {
        private IRepository<Account> Repository { get; }

        public AccountService(IRepository<Account> repository)
        {
            Repository = repository;
        }

        public void AddAccount(Account account)
        {
            if (!CheckNameIsNotNullOrEmpty(account))
            {
                return;
            }

            if (!CheckAgeLimit(account))
            {
                return;
            }
            Repository.Add(account);
        }

        private bool CheckNameIsNotNullOrEmpty(AbstractPerson person)
        {
            return !(string.IsNullOrEmpty(person.FirstName) || string.IsNullOrEmpty(person.LastName));
        }
        private bool CheckAgeLimit(AbstractPerson person)
        {
            return person.BirthDate.AddYears(18) < DateTime.Now;
        }
    }
}
