﻿using System;

namespace Interfaces
{
    public abstract class AbstractPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }

        public AbstractPerson(string firstName, string lastName, DateTime birthDate)
        {
            FirstName = firstName;
            LastName = lastName;
            BirthDate = birthDate;
        }

        public AbstractPerson()
        {

        }

        public override bool Equals(object obj)
        {
            return obj is AbstractPerson person &&
                   FirstName == person.FirstName &&
                   LastName == person.LastName &&
                   BirthDate == person.BirthDate;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(FirstName, LastName, BirthDate);
        }
    }
}
