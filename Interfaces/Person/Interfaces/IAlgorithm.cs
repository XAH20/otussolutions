﻿using System;
using System.Collections.Generic;

namespace Interfaces
{
    public interface IAlgorithm<T> where T : IComparable
    {
        IEnumerable<T> InsertionSort(IEnumerable<T> notSortedItems);
    }
}
