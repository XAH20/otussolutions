﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface ISorter<T>
    {
        IEnumerable<T> SortByBirthDate(IEnumerable<T> notSortedItems);
    }
}
