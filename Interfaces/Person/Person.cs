﻿using System;

namespace Interfaces
{
    public class Person : AbstractPerson, IComparable
    {
        public Person() : base()
        {
        }
        public Person(string firstName, string lastName, DateTime birthDate) : base(firstName, lastName, birthDate)
        {
        }

        public int CompareTo(object other)
        {
            var otherPerson = (Person)other;
            return this.ToString().CompareTo(otherPerson.ToString());
        }

        public override string ToString()
        {
            return $"{LastName} {FirstName} ({BirthDate:yyyy-MM-dd})";
        }
    }
}
