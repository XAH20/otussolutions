﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interfaces
{
    class PersonSorter : ISorter<Person>, IAlgorithm<Person>
    {
        public IEnumerable<Person> SortByBirthDate(IEnumerable<Person> notSortedPersons) 
            => notSortedPersons.OrderBy((person) => (DateTime.Now - person.BirthDate));

        public IEnumerable<Person> InsertionSort(IEnumerable<Person> notSortedPersons)
        {
            var source = notSortedPersons.ToArray();
            var result = new Person[source.Length];
            Array.Copy(source, result, source.Length);
            for (int i = 0; i < result.Length; i++)
            {
                int j = i;
                while (j > 0 && result[j - 1].CompareTo(source[i]) > 0)
                {
                    result[j] = result[j - 1];
                    j--;
                }
                result[j] = source[i];
            }
            return result;
        }
    }
}
