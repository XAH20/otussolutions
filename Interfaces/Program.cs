﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var serializer = new OtusXmlSerializer<Person[]>();
            var newPersons = new List<Person>(){
                new Person("Герберт", "Шилдт", new DateTime(1951, 2, 28)),
                new Person("Марк", "Симан", new DateTime(1960, 1, 1)),
                new Person("Роберт", "Мартин", new DateTime(1952, 1, 1)),
                new Person("Альберт", "Эйнштейн", new DateTime(1879, 3, 14))
            }.ToArray();
            var personsSerialized = serializer.Serialize(newPersons);
            File.WriteAllText("Persons.xml", personsSerialized);

            var persons = new OtusStreamReader<Person>("Persons.xml");

            Console.WriteLine("Persons:");
            foreach (Person person in persons)
            {
                Console.WriteLine(person);
            }

            var personSorter = new PersonSorter();
            var sortedPersons = personSorter.SortByBirthDate(persons);
            Console.WriteLine("\r\nSorted persons by BirthDate:");
            foreach (Person person in sortedPersons)
            {
                Console.WriteLine(person);
            }

            sortedPersons = personSorter.InsertionSort(persons);
            Console.WriteLine("\r\nNew sorted persons by Algorithm:");
            foreach (Person person in sortedPersons)
            {
                Console.WriteLine(person);
            }
        }
    }
}
