﻿namespace Reflection
{
    interface ISerializer<T>
    {
        string Serialize(T item);
        T Deserialize(string file);
    }
}
