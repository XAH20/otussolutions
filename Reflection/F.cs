﻿using Newtonsoft.Json;

namespace Reflection
{
    public class F
    {
        [JsonProperty]
        private int i1;
        [JsonProperty]
        private int i2;
        [JsonProperty]
        private int i3;
        [JsonProperty]
        private int i4;
        [JsonProperty]
        private int i5;

        public static F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 };
    }
}
