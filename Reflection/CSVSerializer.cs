﻿using System;
using System.Text;

namespace Reflection
{
    public class CSVSerializer<T> : ISerializer<T>
    {
        public string Delimiter { get; private set; } = ";";
        public string Serialize(T item)
        {
            var type = item.GetType();
            var fields = type.GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var builder = new StringBuilder();
            foreach (var field in fields)
            {
                builder.Append(field.Name);
                builder.Append(Delimiter);
                builder.Append(field.GetValue(item));
                builder.Append(Environment.NewLine);
            }
            builder.Remove(builder.Length - 2, 2);
            return builder.ToString();
        }

        public T Deserialize(string file)
        {
            var type = typeof(T);
            var newObj = (T)Activator.CreateInstance(type);
            var entrys = file.Split(new string[] { Environment.NewLine }, System.StringSplitOptions.None);
            foreach (var entry in entrys)
            {
                EntryToObject(entry, newObj, type);
            }
            return newObj;
        }

        private void EntryToObject(string entry, T newObj, Type type)
        {
            if (entry == "")
            {
                throw new InvalidOperationException("Empty CSV string");
            }

            var fieldArray = entry.Split(Delimiter);
            if (fieldArray.Length != 2)
            {
                throw new InvalidOperationException("Invalid number of fields in the CSV string");
            }
            var field = type.GetField(fieldArray[0], System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            var fieldType = field.FieldType;
            var fieldValue = GetFieldValue(fieldArray[1], fieldType);
            field.SetValue(newObj, fieldValue);
        }

        private static object GetFieldValue(string fieldValueStr, Type type) => type switch
        {
            _ when type == typeof(int) => int.Parse(fieldValueStr),
            _ when type == typeof(short) => short.Parse(fieldValueStr),
            _ when type == typeof(byte) => byte.Parse(fieldValueStr),
            _ when type == typeof(double) => double.Parse(fieldValueStr),
            _ when type == typeof(bool) => bool.Parse(fieldValueStr),
            _ when type == typeof(char) => char.Parse(fieldValueStr),
            _ => fieldValueStr
        };
    }
}
