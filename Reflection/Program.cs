﻿using System;
using System.Diagnostics;
using Newtonsoft.Json;

namespace Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            var f = F.Get();
            ISerializer<F> serializer = new CSVSerializer<F>();
            var repeats = 100_000;

            ShowTimeSerialization(f, serializer, repeats);
            ShowTimeDeserialization(f, serializer, repeats);
        }

        private static void ShowTimeSerialization(F f, ISerializer<F> serializer, int repeats)
        {
            Console.WriteLine($"Время, затраченное на сериализацию объекта F ({repeats} повторов):");
            var timeSerCustom = CalculateTimeSerialization((x) => serializer.Serialize(x), f, repeats);
            Console.WriteLine($"Кастомный сериализатор: {timeSerCustom} мс");
            var timeSerStandard = CalculateTimeSerialization((x) => JsonConvert.SerializeObject(x), f, repeats);
            Console.WriteLine($"Стандартный сериализатор: {timeSerStandard} мс");
        }

        private static void ShowTimeDeserialization(F f, ISerializer<F> serializer, int repeats)
        {
            var strCustom = serializer.Serialize(f);
            var strStandard = JsonConvert.SerializeObject(f);

            Console.WriteLine($"Время, затраченное на десериализацию объекта F ({repeats} повторов):");
            var timeSerCustom = CalculateTimeDeserialization((x) => serializer.Deserialize(x), strCustom, repeats);
            Console.WriteLine($"Кастомный сериализатор: {timeSerCustom} мс");
            var timeSerStandard = CalculateTimeDeserialization((x) => JsonConvert.DeserializeObject<F>(x), strStandard, repeats);
            Console.WriteLine($"Стандартный сериализатор: {timeSerStandard} мс");
        }

        private static long CalculateTimeSerialization(Func<F, string> funcSerialize, F obj, int repeats)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            string serStr;
            for (int i = 0; i < repeats; i++)
            {
                serStr = funcSerialize(obj);
            }
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }

        private static long CalculateTimeDeserialization(Func<string, F> funcDeserialize, string file, int repeats)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            F obj;
            for (int i = 0; i < repeats; i++)
            {
                obj = funcDeserialize(file);
            }
            stopwatch.Stop();
            return stopwatch.ElapsedMilliseconds;
        }
    }
}
