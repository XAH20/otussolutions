﻿using System;

namespace Multithreading
{
    class Program
    {
        static void Main(string[] args)
        {
            var sumCalculator = new SumCalculator();
            var array = GetArray(100_000);
            sumCalculator.CalculateAndShowSimple(array);
            sumCalculator.CalculateAndShowInThreads(array);
            sumCalculator.CalculateAndShowPlinq(array);
            
            array = GetArray(1_000_000);
            sumCalculator.CalculateAndShowSimple(array);
            sumCalculator.CalculateAndShowInThreads(array);
            sumCalculator.CalculateAndShowPlinq(array);

            array = GetArray(10_000_000);
            sumCalculator.CalculateAndShowSimple(array);
            sumCalculator.CalculateAndShowInThreads(array);
            sumCalculator.CalculateAndShowPlinq(array);
        }

        static int[] GetArray(int numderElements)
        {
            var array = new int[numderElements];
            var random = new Random();
            for (var i = 0; i < array.Length; i++)
            {
                array[i] = random.Next(0, int.MaxValue);
            }

            return array;
        }
    }
}