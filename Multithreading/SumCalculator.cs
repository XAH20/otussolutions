using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Multithreading
{
    public class SumCalculator
    {
        private readonly Stopwatch _stopwatch = new();

        private long _sum;
        private readonly object _sumLocker = new object();
        private readonly int _numberThreads = 5;

        public void CalculateAndShowSimple(int[] array)
        {
            CalculateAndShow(SumArraySimple, array);
        }
        
        public void CalculateAndShowInThreads(int[] array)
        {
            CalculateAndShow(SumArrayThreads, array);
        }
        
        public void CalculateAndShowPlinq(int[] array)
        {
            CalculateAndShow(SumArrayPlinq, array);
        }
        
        private void CalculateAndShow(Func<int[], long> method, int[] array)
        {
            Console.WriteLine($"Start method {method.Method.Name}...");
            _stopwatch.Restart();
            var sum = method(array);
            _stopwatch.Stop();
            Console.WriteLine($"Sum = {sum}");
            Console.WriteLine($"Method execution time {method.Method.Name} with {array.Length} elements: {_stopwatch.ElapsedMilliseconds} ms ({_stopwatch.ElapsedTicks} ticks)");
        }

        private long SumArraySimple(int[] array)
        {
            return array.Sum(x => (long)x);
        }

        private long SumArrayThreads(int[] array)
        {
            _sum = 0;
            var threadList = new List<Thread>();
            var chunkLength = (int)Math.Ceiling((double)array.Length / _numberThreads);
            for (var i = 0; i < _numberThreads; i++)
            {
                var thread = new Thread(SumArrayInThread);
                threadList.Add(thread);
                var chunkArray = array.Skip(i * chunkLength).Take(chunkLength).ToArray();
                thread.Start(chunkArray);
            }

            foreach (var thread in threadList)
            {
                thread.Join();
            }

            return _sum;
        }
        
        private void SumArrayInThread(object array)
        {
            long sum = ((int[])array).Sum(x => (long)x);
            lock (_sumLocker)
            {
                _sum += sum;
            }
        }

        private long SumArrayPlinq(int[] array)
        {
            return array.AsParallel().Sum(x => (long)x);
        }
    }
}