﻿using System;
using System.Collections.Generic;
using Prototype.Domain;
using OperatingSystem = Prototype.Domain.OperatingSystem;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var processor = new Component(Guid.NewGuid(),"Processor", "Intel", "Pentium i7");
            var ram = new Component(Guid.NewGuid(),"RAM", "HyperX", "32 GB DDR4");
            var ssd = new Component(Guid.NewGuid(),"SSD", "Samsung", "1 TB");

            var systemUnit = new Component(Guid.NewGuid(),"System unit", "Noname", "X", new List<Component>() {processor, ram, ram, ssd});

            var display = new Component(Guid.NewGuid(),"Display", "Samsung", "4к");
            var mouse = new Component(Guid.NewGuid(),"Mouse", "Genius", "USB");
            var audioHeadset = new Component(Guid.NewGuid(),"Audio headset", "Logitech", "1");
            var keyboard = new Component(Guid.NewGuid(),"Keyboard", "Logitech", "USB");

            var operatingSystem = new OperatingSystem(Guid.NewGuid(), "Windows", "10 X64");
            
            var pc = new PersonalComputer(Guid.NewGuid(),"Desktop", "Noname", "desktop", operatingSystem, 
                "Intel Pentium i7, 64 GB RAM, 1 GB SSD", new List<Component>() {systemUnit, display, mouse, audioHeadset, keyboard});

            var copyPc1 = (PersonalComputer)pc.Copy();
            var copyPc2 = (PersonalComputer)pc.Clone();

            Console.WriteLine($"PC:\n{pc}");
            Console.WriteLine();
            Console.WriteLine($"Copy PC 1:\n{copyPc1}");
            Console.WriteLine();
            Console.WriteLine($"Copy PC 2:\n{copyPc2}");


        }
    }
}
