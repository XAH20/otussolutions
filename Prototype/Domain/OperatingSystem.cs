using System;
using System.Text.Json;
using Prototype.Interfaces;

namespace Prototype.Domain
{
    public class OperatingSystem : IPrototype<OperatingSystem>, ICloneable
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Version { get; }

        public OperatingSystem(Guid id, string name, string version)
        {
            Id = id;
            Name = name;
            Version = version;
        }
        
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
        
        public OperatingSystem Copy()
        {
            return new (Guid.NewGuid(), Name, Version);
        }

        public object Clone()
        {
            return Copy();
        }
    }
}