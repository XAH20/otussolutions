﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Prototype.Interfaces;

namespace Prototype.Domain
{
    /// <summary>
    /// Персональный компьютер
    /// </summary>
    public class PersonalComputer : CompositeDevice
    {
        public OperatingSystem OperatingSystem { get; }
        public string Specifications { get; }

        public PersonalComputer(Guid id, string name, string manufacturer, string model, OperatingSystem operatingSystem, string specifications, List<Component> components = null) : base(id, name, manufacturer, model, components)
        {
            OperatingSystem = operatingSystem;
            Specifications = specifications;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
        
        public override Device Copy()
        {
            return new PersonalComputer(Guid.NewGuid(), Name, Manufacturer, Model, OperatingSystem.Copy(),
                Specifications, Components.Select(x => (Component)x.Copy()).ToList());
        }

        public override object Clone()
        {
            return Copy();
        }
    }
}
