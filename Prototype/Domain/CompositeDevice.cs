﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Prototype.Interfaces;

namespace Prototype.Domain
{
    /// <summary>
    /// Составное устройство, состоящее из нескольких компонентов
    /// </summary>
    public class CompositeDevice : Device
    {
        public List<Component> Components { get; }
        public CompositeDevice(Guid id, string name, string manufacturer, string model, List<Component> components = null) : base(id, name, manufacturer, model)
        {
            Components = components;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
        
        public override Device Copy()
        {
            return new CompositeDevice(Guid.NewGuid(), Name, Manufacturer, Model, Components.Select(x => (Component)x.Copy()).ToList());
        }
        
        public override object Clone()
        {
            return Copy();
        }
    }
}
