﻿using System;
using System.Text.Json;
using Prototype.Interfaces;

namespace Prototype.Domain
{
    /// <summary>
    /// Любое устройство
    /// </summary>
    public class Device : IPrototype<Device>, ICloneable
    {
        public Guid Id { get; }
        public string Name { get; }
        public string Manufacturer { get; }
        public string Model { get; }

        public Device(Guid id, string name, string manufacturer, string model)
        {
            Id = id;
            Name = name;
            Manufacturer = manufacturer;
            Model = model;
        }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
        
        public virtual Device Copy()
        {
            return new (Guid.NewGuid(), Name, Manufacturer, Model);
        }

        public virtual object Clone()
        {
            return Copy();
        }
    }
}
