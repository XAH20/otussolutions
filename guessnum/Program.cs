﻿namespace guessnum
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleDataReader consoleDataReader = new ();
            ConsoleDataWriter consoleDataWriter = new ();
            MyGameSettings myGameSettings = new() { AttemptsNumber = 3 };
            MyGeneratorSettings myGeneratorSettings = new() {MinValue = 1, MaxValue = 10};
            NumberGenerator numberGenerator = new(myGeneratorSettings);
            Game game = new(consoleDataReader, consoleDataWriter, myGameSettings, numberGenerator);

            game.Play();
        }
    }
}
