﻿namespace guessnum.Interfaces
{
    public abstract class GameSettings
    {
        public byte AttemptsNumber { get; set; }
    }
}
