﻿namespace guessnum.Interfaces
{
    public abstract class GeneratorSettings
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public virtual string MinMaxCondition { get; } = "MinValue < MaxValue";

        public virtual bool CheckMinMaxValues() => MinValue < MaxValue;
    }
}
