﻿using System;
using System.IO;
using guessnum.Interfaces;

namespace guessnum
{
    class NumberGenerator : IIntervalNumberGenerator
    {
        private readonly GeneratorSettings _generatorSettings;
        private readonly Random _random;

        public NumberGenerator(GeneratorSettings generatorSettings)
        {
            _generatorSettings = generatorSettings;
            CheckSettings();
            _random = new Random();
        }

        public int GenerateInt()
        {
            return _random.Next(_generatorSettings.MinValue, _generatorSettings.MaxValue + 1);
        }

        public int GetMinValue() => _generatorSettings.MinValue;

        public int GetMaxValue() => _generatorSettings.MaxValue;

        private void CheckSettings()
        {
            if (!_generatorSettings.CheckMinMaxValues())
            {
                throw new InvalidDataException($"Invalid generator settings: min value and max value do not meet the condition \"{_generatorSettings.MinMaxCondition}\"");
            }
        }
    }
}
