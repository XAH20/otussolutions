﻿using System;
using guessnum.Interfaces;

namespace guessnum
{
    class ConsoleDataWriter : IDataWriter
    {
        public void WriteLine(string text)
        {
            Console.WriteLine(text);
        }

        public void Write(string text)
        {
            Console.Write(text);
        }
    }
}
