﻿using guessnum.Interfaces;

namespace guessnum
{
    class MyGeneratorSettings : GeneratorSettings
    {
        public override string MinMaxCondition { get; } = "MinValue + 1 < MaxValue";

        public override bool CheckMinMaxValues() => MinValue + 1 < MaxValue;
    }
}
