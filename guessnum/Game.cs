﻿using guessnum.Interfaces;

namespace guessnum
{
    class Game
    {
        private readonly IDataReader _dataReader;
        private readonly IDataWriter _dataWriter;
        private readonly GameSettings _gameSettings;
        private readonly IIntervalNumberGenerator _numberGenerator;

        public Game(IDataReader dataReader, IDataWriter dataWriter, GameSettings gameSettings, IIntervalNumberGenerator numberGenerator)
        {
            _dataReader = dataReader;
            _dataWriter = dataWriter;
            _gameSettings = gameSettings;
            _numberGenerator = numberGenerator;
        }

        public void Play()
        {
            WriteStartMessage();
            var generatedNumber = _numberGenerator.GenerateInt();
            for (var i = 0; i < _gameSettings.AttemptsNumber; i++)
            {
                WriteNextAttemptMessage(i + 1);
                var userNumber = ReadNumber();
                if (userNumber == generatedNumber)
                {
                    WriteWinMessage();
                    return;
                }

                WriteMissMessage(generatedNumber, userNumber);
            }
            WriteLoseMessage(generatedNumber);
        }

        private void WriteStartMessage()
        {
            _dataWriter.WriteLine(
                $"Guess a number from {_numberGenerator.GetMinValue()} to {_numberGenerator.GetMaxValue()} in {_gameSettings.AttemptsNumber} attempts!");
        }

        private void WriteMissMessage(int generatedNumber, int userNumber)
        {
            _dataWriter.WriteLine("Unsuccessful attempt");
            var comparison = userNumber < generatedNumber ? "LESS" : "GREATER";
            _dataWriter.WriteLine($"The entered number is {comparison} than the guessed number");
        }

        private void WriteNextAttemptMessage(int attemptNumber)
        {
            _dataWriter.WriteLine("");
            _dataWriter.WriteLine($"Attempt # {attemptNumber} of {_gameSettings.AttemptsNumber}");
            _dataWriter.Write("Enter the number: ");
        }

        private int ReadNumber()
        {
            var number = _dataReader.ReadInt();
            _dataWriter.WriteLine($"You entered: {number}");
            return number;
        }

        private void WriteWinMessage()
        {
            _dataWriter.WriteLine("");
            _dataWriter.WriteLine("You win!!!!");
        }

        private void WriteLoseMessage(int generatedNumber)
        {
            _dataWriter.WriteLine("");
            _dataWriter.WriteLine("You lose...");
            _dataWriter.WriteLine($"Guessed number: {generatedNumber}");
        }
    }
}