﻿namespace guessnum.Interfaces
{
    public interface IIntervalNumberGenerator : INumberGenerator, IDataMinMaxValue
    {
    }
}
