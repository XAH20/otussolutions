﻿namespace guessnum.Interfaces
{
    public interface IDataMinMaxValue
    {
        int GetMinValue();
        int GetMaxValue();
    }
}
