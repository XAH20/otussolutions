﻿namespace guessnum.Interfaces
{
    public interface INumberGenerator
    {
        int GenerateInt();
    }
}
