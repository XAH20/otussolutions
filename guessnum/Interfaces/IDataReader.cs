﻿namespace guessnum.Interfaces
{
    public interface IDataReader
    {
        int ReadInt();
    }
}
