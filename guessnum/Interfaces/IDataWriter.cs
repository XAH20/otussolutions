﻿namespace guessnum.Interfaces
{
    public interface IDataWriter
    {
        void WriteLine(string text);
        void Write(string text);
    }
}
