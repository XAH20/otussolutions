﻿using System;
using guessnum.Interfaces;

namespace guessnum
{
    public class ConsoleDataReader : IDataReader
    {
        public int ReadInt() => int.Parse(Console.ReadLine()!);
    }
}
