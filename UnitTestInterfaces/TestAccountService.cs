using Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestInterfaces
{
    [TestClass]
    public class TestAccountService
    {
        private static List<Account> _accounts = new List<Account>();

        [TestMethod]
        public void TestAddAccount()
        {
            var accountExpected = new Account("�������", "��������", new DateTime(1879, 3, 14));
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(x => x.Add(accountExpected)).Callback(new InvocationAction((x) => Add(accountExpected)));

            var accountService = new AccountService(mock.Object);
            accountService.AddAccount(accountExpected);
            var accountActual = _accounts.FirstOrDefault();

            Assert.IsNotNull(accountActual);
            Assert.AreEqual(accountExpected.FirstName, accountActual.FirstName);
            Assert.AreEqual(accountExpected.LastName, accountActual.LastName);
            Assert.AreEqual(accountExpected.BirthDate.Ticks, accountActual.BirthDate.Ticks);
        }

        [TestMethod]
        public void TestAddAccountYounger18()
        {
            _accounts.Clear();
            var accountExpected = new Account("����", "������", new DateTime(2005, 12, 12));
            var mock = new Mock<IRepository<Account>>();
            mock.Setup(x => x.Add(accountExpected)).Callback(new InvocationAction((x) => Add(accountExpected)));

            var accountService = new AccountService(mock.Object);
            accountService.AddAccount(accountExpected);
            var accountActual = _accounts.FirstOrDefault();

            Assert.IsNull(accountActual);
        }

        private void Add(Account account)
        {
            _accounts.Add(account);
        }
    }
}
