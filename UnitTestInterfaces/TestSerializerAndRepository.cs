﻿using Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;

namespace UnitTestInterfaces
{
    [TestClass]
    public class TestSerializerAndRepository
    {
        private static readonly IRepository<Account> repo = new Repository("Accounts.xml");
        private static readonly IAccountService accountService = new AccountService(repo);
        private static readonly Account accountMarkSiman = new Account("Марк", "Симан", new DateTime(1960, 1, 1));

        [TestMethod]
        public void TestAddAndGetAccounts()
        {
            File.Delete("Accounts.xml");
            var accountsExpected = new Account[] 
            { 
                new Account("Герберт", "Шилдт", new DateTime(1951, 2, 28)),
                accountMarkSiman,
                new Account("Роберт", "Мартин", new DateTime(1952, 1, 1)),
                new Account("Альберт", "Эйнштейн", new DateTime(1879, 3, 14))
            };

            foreach(Account account in accountsExpected)
            {
                accountService.AddAccount(account);
            }
            var accountsActual = repo.GetAll().ToArray();

            for (var i = 0; i < accountsExpected.Length; i++)
            {
                Assert.AreEqual(accountsExpected[i], accountsActual[i]);
            }
        }

        [TestMethod]
        public void TestGetOneAccount()
        {
            var accountActual = repo.GetOne((account) => account.FirstName == "Марк");

            Assert.AreEqual(accountMarkSiman, accountActual);
        }
    }
}
