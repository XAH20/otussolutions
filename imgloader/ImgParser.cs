﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace imgparser
{
    class ImgParser : IImgParser
    {
        private readonly IUriDownloader _uriDownloader;
        private const string PATTERN_URI_IMG = "<img .*?src=\"(?<img>(https?:\\/\\/|\\/\\/|\\/)[^;]+?)\"";
        private readonly Regex _regex = new Regex(PATTERN_URI_IMG);

        public ImgParser(IUriDownloader uriDownloader)
        {
            _uriDownloader = uriDownloader;
        }

        public List<string> ParseImage(string uri)
        {
            var http = _uriDownloader.DownloadString(uri);

            return ParseImg(http);
        }

        private List<string> ParseImg(string http)
        {
            var images = new List<string>();
            var matchImg = _regex.Match(http);
            while (matchImg.Success)
            {
                var urlImg = matchImg.Groups["img"].Value;
                images.Add(urlImg);
                matchImg = matchImg.NextMatch();
            }
            return images;
        }


    }
}
