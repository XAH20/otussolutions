﻿using System;
using System.Net;

namespace imgparser
{
    public class UriDownloader : IUriDownloader
    {
        public string DownloadString(string uri)
        {
            var webClient = CreateWebClient();
            var data = webClient.DownloadString(uri);
            return data;
        }

        public void DownloadFile(Uri uri, string filename)
        {
            var webClient = CreateWebClient();
            webClient.DownloadFileAsync(uri, filename);
        }

        private WebClient CreateWebClient()
        {
            using var client = new WebClient();
            client.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36");
            return client;
        }
    }
}
