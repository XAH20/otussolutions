﻿using System;
using System.Collections.Generic;
using System.IO;

namespace imgparser
{
    class Program
    {
        static void Main(string[] args)
        {
            var downloader = new UriDownloader();
            var parser = new ImgParser(downloader);

            string uri;
            try
            {
                uri = ReadArgs(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Using: imgloader <uri>");
                return;
            }

            var images = parser.ParseImage(uri);

            Console.WriteLine($"Uri images in {uri}:");
            foreach (var img in images)
            {
                Console.WriteLine(img);
            }

            DownloadImages(downloader, uri, images);
        }

        private static string ReadArgs(string[] args)
        {
            if (args.Length == 0 || args.Length > 1)
            {
                throw new InvalidOperationException("Application launch parameters are not meeting expectations");
            }
            return args[0];
        }

        private static void DownloadImages(IUriDownloader downloader, string uri, List<string> images)
        {
            var outDir = CreateDir(uri);

            DownloadImagesInDir(downloader, images, outDir, uri);

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"\r\nImages writed in {outDir.FullName}");
            Console.ResetColor();
            Console.WriteLine("\r\nPress Enter to close app...");
            Console.ReadKey();
        }

        private static DirectoryInfo CreateDir(string uri)
        {
            var dirname = string
                .Join("_", uri.Split(Path.GetInvalidFileNameChars()))
                .Replace("/", "_")
                .Insert(0, "images_");
            dirname = dirname.Remove(dirname.LastIndexOf(".", StringComparison.Ordinal));

            var dir = new DirectoryInfo(dirname);
            if (!dir.Exists)
            {
                dir.Create();
            }

            return dir;
        }

        private static void DownloadImagesInDir(IUriDownloader downloader, List<string> images, DirectoryInfo outDir, string uri)
        {
            foreach (var img in images)
            {
                try
                {
                    var filenameFull = ВuildFileName(img, outDir);
                    var modImgUri = ModifyImagUri(img, uri);
                    Console.WriteLine($"Loading image: {img}");
                    var imgUri = new Uri(modImgUri);
                    downloader.DownloadFile(imgUri, filenameFull);
                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine($"Image {img} download error: {e.Message}");
                    Console.ResetColor();
                }
            }
        }

        private static string ВuildFileName(string imgUri, DirectoryInfo outDir)
        {
            var filename = imgUri
                .Remove(0, imgUri.LastIndexOf("/", StringComparison.Ordinal) + 1);
            if (filename.Contains("?"))
            {
                filename = filename.Remove(filename.IndexOf("?", StringComparison.Ordinal));
            }
            var filenameFull = $"{outDir.FullName}/{filename}";
            while (File.Exists(filenameFull))
            {
                filenameFull = filenameFull.Insert(filenameFull.LastIndexOf("."), "_");
            }

            return filenameFull;
        }

        private static string ModifyImagUri(string oldImagUri, string uri)
        {
            var newImgUri = oldImagUri;
            if (oldImagUri.StartsWith("//"))
            {
                newImgUri = newImgUri.Insert(0, "http:");
            }
            else if (oldImagUri.StartsWith("/"))
            {
                var u = new Uri(uri);
                newImgUri = newImgUri.Insert(0, $"{u.Scheme}://{u.Host}");
            }
            return newImgUri;
        }
    }
}
