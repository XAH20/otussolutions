﻿using System.Collections.Generic;

namespace imgparser
{
    public interface IImgParser
    {
        List<string> ParseImage(string uri);
    }
}
