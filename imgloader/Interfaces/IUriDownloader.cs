﻿using System;

namespace imgparser
{
    interface IUriDownloader
    {
        string DownloadString(string uri);

        void DownloadFile(Uri uri, string filename);
    }
}
