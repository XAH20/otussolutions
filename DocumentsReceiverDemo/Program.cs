﻿using System;

namespace DocumentsReceiverDemo
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string[] filesNames = { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
            var targetDirectory = @"D:\Files";
            double waitingInterval = 60_000;
            using var documentsReceiver = new DocumentsReceiver.DocumentsReceiver(filesNames);
            documentsReceiver.DocumentsReady += DocumentsReady;
            documentsReceiver.TimedOut += TimedOut;
            documentsReceiver.Start(targetDirectory, waitingInterval);
            Console.ReadKey();
        }

        private static void DocumentsReady(object sender, EventArgs e)
        {
            Console.WriteLine("All documents loaded");
        }

        private static void TimedOut(object sender, EventArgs e)
        {
            Console.WriteLine("Timed out");
        }
    }
}
