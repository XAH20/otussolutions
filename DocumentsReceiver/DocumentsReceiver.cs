﻿using System;
using System.IO;
using System.Linq;
using System.Timers;

namespace DocumentsReceiver
{
    public class DocumentsReceiver : IDisposable
    {
        public event DocumentsReadyEventHandler DocumentsReady;
        public event TimedOutEventHandler TimedOut;

        public delegate void DocumentsReadyEventHandler(object sender, EventArgs e);
        public delegate void TimedOutEventHandler(object sender, EventArgs e);

        private readonly string[] _filesNames;
        private readonly FileSystemWatcher _fileSystemWatcher;
        private readonly Timer _timer;

        private string[] _observedFilesFullNames;

        public DocumentsReceiver(string[] filesNames)
        {
            _filesNames = filesNames;
            _fileSystemWatcher = new FileSystemWatcher();
            _timer = new Timer();
        }

        public void Start(string targetDirectory, double waitingInterval)
        {
            _observedFilesFullNames = _filesNames.Select(f => $"{targetDirectory}\\{f}").ToArray();

            _fileSystemWatcher.Path = targetDirectory;
            _fileSystemWatcher.EnableRaisingEvents = true;
            _fileSystemWatcher.NotifyFilter = NotifyFilters.FileName;
            _fileSystemWatcher.Created += OnCreatedOrRenamed;
            _fileSystemWatcher.Renamed += OnCreatedOrRenamed;

            _timer.Interval = waitingInterval;
            _timer.Elapsed += OnElapsed;
            _timer.Start();
        }

        private void OnCreatedOrRenamed(object sender, FileSystemEventArgs e)
        {
            if (_observedFilesFullNames.All(File.Exists))
            {
                DocumentsReady?.Invoke(this, null);
                Stop();
            }
        }

        private void OnElapsed(object sender, ElapsedEventArgs e)
        {
            TimedOut?.Invoke(this, null);
            Stop();
        }

        private void Stop()
        {
            _fileSystemWatcher.Created -= OnCreatedOrRenamed;
            _fileSystemWatcher.Renamed -= OnCreatedOrRenamed;
            _timer.Elapsed -= OnElapsed;
            _timer.Stop();
            _timer.Interval = 0;
            _fileSystemWatcher.Path = null;
        }

        public void Dispose()
        {
            _fileSystemWatcher?.Dispose();
            _timer?.Close();
        }
    }
}
